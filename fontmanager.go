//++++++++++++++++++++++++++++++++++++++++
//Fighting for great,share generate value!
//Build the best soft by golang,let's go!
//++++++++++++++++++++++++++++++++++++++++
//Author:ShirDon <http://www.shirdon.com>
//Email:hcbsts@163.com;  823923263@qq.com
//++++++++++++++++++++++++++++++++++++++++

package captchas_with_go

import (
	"github.com/golang/freetype/truetype"
	"io/ioutil"
	"math/rand"
	"time"
)

//FontManager is a Font Manager the manage font list
type FontManager struct {
	fontFiles   []string
	fontObjects map[string]*truetype.Font
	randObject  *rand.Rand
}

//CreateFontManager will create a new Font Manager
func CreateFontManager() *FontManager {
	fm := new(FontManager)
	fm.fontFiles = []string{}
	fm.fontObjects = make(map[string]*truetype.Font)
	fm.randObject = rand.New(rand.NewSource(time.Now().UnixNano()))

	return fm
}

//AddFont will add a new font file to the list
func (fm *FontManager) AddFont(pathToFontFile string) error {
	fontBytes, err := ioutil.ReadFile(pathToFontFile)
	if err != nil {
		return err
	}

	font, err := truetype.Parse(fontBytes)

	if err != nil {
		return err
	}
	fm.fontFiles = append(fm.fontFiles, pathToFontFile)
	fm.fontObjects[pathToFontFile] = font

	return nil
}

//GetFont will return a Font struct by path
func (fm *FontManager) GetFont(pathToFontFile string) *truetype.Font {
	return fm.fontObjects[pathToFontFile]
}

//GetRandomFont will return a random Font struct
func (fm *FontManager) GetRandomFont() *truetype.Font {
	randomIndex := fm.randObject.Intn(len(fm.fontFiles))
	fontFile := fm.fontFiles[randomIndex]
	rst := fm.GetFont(fontFile)

	return rst
}
