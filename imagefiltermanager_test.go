//++++++++++++++++++++++++++++++++++++++++
//Fighting for great,share generate value!
//Build the best soft by golang,let's go!
//++++++++++++++++++++++++++++++++++++++++
//Author:ShirDon <http://www.shirdon.com>
//Email:hcbsts@163.com;  823923263@qq.com
//++++++++++++++++++++++++++++++++++++++++

package captchas_with_go

import (
	"testing"
)

func TestImageFilterManager(t *testing.T) {

	manager := CreateImageFilterManager()
	manager.AddFilter(&ImageFilterBase{})
	if 1 != len(manager.GetFilters()) {
		t.Error("add or get failed")
	}
}

func TestImageFilterManagerConfig(t *testing.T) {

	filterConfig := new(FilterConfig)
	filterConfig.Init()
	filterConfig.Filters = []string{"ImageFilterNoiseLine", "ImageFilterNoisePoint", "ImageFilterStrike"}
	var filterConfigGroup *FilterConfigGroup
	filterConfigGroup = new(FilterConfigGroup)
	filterConfigGroup.Init()
	filterConfigGroup.SetItem("Num", "5")
	filterConfig.SetGroup("ImageFilterNoiseLine", filterConfigGroup)
	filterConfigGroup = new(FilterConfigGroup)
	filterConfigGroup.Init()
	filterConfigGroup.SetItem("Num", "10")
	filterConfig.SetGroup("ImageFilterNoisePoint", filterConfigGroup)
	filterConfigGroup = new(FilterConfigGroup)
	filterConfigGroup.Init()
	filterConfigGroup.SetItem("Num", "3")
	filterConfig.SetGroup("ImageFilterStrike", filterConfigGroup)

	manager := CreateImageFilterManagerByConfig(filterConfig)
	filters := manager.GetFilters()

	if 3 != len(filters) {
		t.Error("create by config failed")
	}

	for _, filter := range filters {
		config := filter.GetConfig()
		var num string
		switch filter.GetId() {
		case "ImageFilterNoiseLine":

			if num, _ = config.GetItem("Num"); "5" != num {
				t.Error("ImageFilterNoiseLine ERROR")
			}
			break
		case "ImageFilterNoisePoint":
			if num, _ = config.GetItem("Num"); "10" != num {
				t.Error("ImageFilterNoisePoint ERROR")
			}
			break
		case "ImageFilterStrike":
			if num, _ = config.GetItem("Num"); "3" != num {
				t.Error("ImageFilterStrike ERROR")
			}
			break
		}

	}

}
