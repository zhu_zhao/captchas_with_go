//++++++++++++++++++++++++++++++++++++++++
//Fighting for great,share generate value!
//Build the best soft by golang,let's go!
//++++++++++++++++++++++++++++++++++++++++
//Author:ShirDon <http://www.shirdon.com>
//Email:hcbsts@163.com;  823923263@qq.com
//++++++++++++++++++++++++++++++++++++++++

package captchas_with_go

import (
	"strconv"
)

//ImageFilter is the interface of image filter
const IMAGE_FILTER_NOISE_LINE = "ImageFilterNoiseLine"

type ImageFilterNoiseLine struct {
	ImageFilterBase
}

func imageFilterNoiseLineCreator(config FilterConfigGroup) ImageFilter {
	filter := ImageFilterNoiseLine{}
	filter.SetConfig(config)
	return &filter
}

//Proc the image
func (filter *ImageFilterNoiseLine) Proc(cimage *CImage) {

	var num int
	var err error
	v, ok := filter.config.GetItem("Num")
	if ok {
		num, err = strconv.Atoi(v)
		if nil != err {
			num = 3
		}
	} else {
		num = 3
	}

	for i := 0; i < num; i++ {
		x := rnd(0, cimage.Bounds().Max.X)
		cimage.drawHorizLine(int(float32(x)/1.5), x, rnd(0, cimage.Bounds().Max.Y), uint8(rnd(1, colorCount)))
	}
}

func (filter *ImageFilterNoiseLine) GetId() string {
	return IMAGE_FILTER_NOISE_LINE
}
